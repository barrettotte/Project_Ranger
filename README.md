# Project_Ranger
[![pipeline-status](https://gitlab.com/barrettotte/Project_Ranger/badges/master/pipeline.svg)](https://gitlab.com/barrettotte/Project_Ranger/pipelines)
[![open-issues](https://img.shields.io/github/issues/barrettotte/Project_Ranger.svg)](https://gitlab.com/barrettotte/Project_Ranger/boards/)
[![closed-issues](https://img.shields.io/github/issues-closed/barrettotte/Project_Ranger.svg)](https://gitlab.com/barrettotte/Project_Ranger/boards/)
[![tag](https://img.shields.io/github/tag/barrettotte/Project_Ranger.svg)](https://github.com/barrettotte/Project_Ranger//releases)
[![downloads](https://img.shields.io/github/downloads/barrettotte/Project_Ranger/total.svg)](https://github.com/barrettotte/Project_Ranger//releases)

My main game development project, named after my cat.

![latest-screenshot](https://github.com/barrettotte/Project_Ranger/blob/master/Files/Screenshots/latest.png)

## Links
* [GitHub.io Page](https://barrettotte.github.io/Project_Ranger/)
* [GitHub Repo](https://github.com/barrettotte/Project_Ranger/)
* [GitLab Repo](https://gitlab.com/barrettotte/Project_Ranger/)
* [Issue Board](https://gitlab.com/barrettotte/Project_Ranger/boards/)
* [DockerHub Repo](https://hub.docker.com/u/barrettotte/)
* [Imgur Album](https://imgur.com/a/xGWNicy)
* [ArtStation Page](https://www.artstation.com/barrettotte)
* [WakaTime Stats](https://wakatime.com/@barrettotte)

## Build Artifacts
* [Linux](https://gitlab.com/barrettotte/Project_Ranger/-/jobs/100572763/artifacts/browse/Builds/)
* [OSX](https://gitlab.com/barrettotte/Project_Ranger/-/jobs/100572764/artifacts/browse/Builds/)
* [Windows](https://gitlab.com/barrettotte/Project_Ranger/-/jobs/100572765/artifacts/browse/Builds/)

## Changelog
<pre>
 <h3>V0.05 - Deciding on new Architecture/Design Pattern and GitHub.io Page Content</h3>
   01.   Make the big decision on the new architecture/design pattern to implement
   02.   Link Doxygen documentation to GitHub.io page's documentation
   03.   Add XML export to timesheet and changelog generator
   04.   Make build artifacts from last successful pipeline downloadable and link it on repo
   05.   Create CI/CD pipeline diagram and add to documentation
   06.   Write Dev Blog for Unity CI Setup
   07.   Repair Github.io page and design dev blog posts
   08.   Remove Gem theme dependency from GitHub.io page
   09.   Create script for generating basic README file including the change log, time sheet, etc
   10.   Create backup script for zipping project and saving to specified path
   11.   Add markdown export for TimeSheet script for Totals
   12.   Add home page content to Github.io page
   13.   Add content to changelog page
   14.   Add to documentation page: Workflow, relevant information, etc
   15.   Populate Github.io page with relevant information about the project
   16.   Start redesigning project for new design pattern
   Start: 2018-09-12  ---  End: 2018-09-19
</pre>



## Project Ranger Time Sheet

#### 2018-08-30 - 2018-09-23
| **Application** | **Time**            |
| --------------- | ------------------- |
| Total Google Chrome  | 06:04:04      |
| Total Oracle VM VirtualBox  | 13:12:20      |
| Total Blender  | 00:02:49      |
| Total Adobe Photoshop CS6  | 00:15:57      |
| Total Unity  | 05:22:17      |
| Total Visual Studio Code  | 13:56:07      |
| Total Microsoft Visio  | 01:02:17      |
|      Total      | **39:55:51**  |


