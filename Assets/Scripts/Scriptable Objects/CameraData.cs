using UnityEngine;
using System;

[CreateAssetMenu(fileName = "CameraData", menuName = "Project_Ranger/CameraData")]

public class CameraData : ScriptableObject {

    public Vector2 cameraSensitivity;
    public Vector2 cameraRotationMaxima;
    public Vector2 cameraRotationMinima;

    public bool useVerticalClamp;
    public bool useCameraSmooth;
    public float cameraSmoothFactor;
    
}