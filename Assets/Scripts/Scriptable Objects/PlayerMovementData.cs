using UnityEngine;
using System;

[CreateAssetMenu(fileName = "PlayerMovementData", menuName = "Project_Ranger/MovementData")]

public class PlayerMovementData : ScriptableObject {
    
    public float walkSpeed;

}