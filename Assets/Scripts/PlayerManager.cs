using UnityEngine;
using System.Collections.Generic;

public class PlayerManager : MonoBehaviour {
    
    [Header("Events")]
    public GameEvent inputEvent;

    private Transform systemsObj;
    private Transform systemListenersObj;

    private CameraSystem cameraSystem;
    private MovementSystem movementSystem;


    private void Awake() {
        systemsObj = this.transform.Find("Systems");
        systemListenersObj = this.transform.Find("System Listeners");
        cameraSystem = systemsObj.GetComponent<CameraSystem>();
        movementSystem = systemsObj.GetComponent<MovementSystem>();
    }

    public void Tick(){
        CheckInput();
        movementSystem.Tick();
        cameraSystem.Tick();
    }

    private void CheckInput(){
        if(Input.anyKeyDown){
            inputEvent.Raise();
        }
    }
}