using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
    
    [Header("Data Assets")]
    [SerializeField]
    private GameManagerData gmData;

    [SerializeField]
    private List<PlayerManager> allPlayers;

    private void Awake(){
        InvokeRepeating("CheckForPlayers", 0.1f, 2.0f);
    }

    private void Update(){
        for(int i = 0; i < allPlayers.Count; i++){
            allPlayers[i].Tick();
        }
    }

    private void CheckForPlayers(){
        allPlayers = new List<PlayerManager>(FindObjectsOfType<PlayerManager>());
        if(allPlayers.Count == 0){
            print("GM-Error: No Player Managers found.");
        }
    }

}