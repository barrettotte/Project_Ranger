using UnityEngine;

public class CameraSystem : MonoBehaviour {
    
    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private Transform playerBody;
    [SerializeField]
    private CameraData cameraData;

    private Quaternion playerTargetRotation;
    private Quaternion cameraTargetRotation;


    private void Awake(){
        playerTargetRotation = playerBody.localRotation;
		cameraTargetRotation = mainCamera.transform.localRotation;
    }

    public void Tick(){
        DoLookRotation();
    }

    private void DoLookRotation(){
        float yRot = Input.GetAxisRaw("Mouse X") * cameraData.cameraSensitivity.x;
		float xRot = Input.GetAxisRaw("Mouse Y") * cameraData.cameraSensitivity.y;
        playerTargetRotation *= Quaternion.Euler (0f, yRot, 0f);
        cameraTargetRotation *= Quaternion.Euler(-xRot, 0f, 0f);

        if(cameraData.useVerticalClamp){
            cameraTargetRotation = ClampRotationAroundXAxis(cameraTargetRotation);
        }
        if(cameraData.useCameraSmooth){
			playerBody.localRotation = Quaternion.Slerp(
                playerBody.localRotation,
                playerTargetRotation,
                cameraData.cameraSmoothFactor * Time.deltaTime
            );
			mainCamera.transform.localRotation = Quaternion.Slerp(
                mainCamera.transform.localRotation,
                cameraTargetRotation,
                cameraData.cameraSmoothFactor * Time.deltaTime
            );
		}
		else{
			playerBody.localRotation = playerTargetRotation;
			mainCamera.transform.localRotation = cameraTargetRotation;
		}
    }

    private Quaternion ClampRotationAroundXAxis(Quaternion q){
		q.x /= q.w;
		q.y /= q.w;
		q.z /= q.w;
		q.w = 1.0f;
		float angleX = Mathf.Clamp(
            2.0f * Mathf.Rad2Deg * Mathf.Atan (q.x),
            cameraData.cameraRotationMinima.x,
            cameraData.cameraRotationMaxima.x
        );
		q.x = Mathf.Tan (0.5f * Mathf.Deg2Rad * angleX);
		return q;
	}


}