using UnityEngine;

public class MovementSystem : MonoBehaviour {

    [SerializeField]
    private PlayerMovementData movementData;
    [SerializeField]
    private CharacterController charController;
    
    private Transform playerTransform;
    private float rotationY;
	private Vector3 moveDirection;

    
    public void Awake(){
        playerTransform = charController.transform;
        moveDirection = Vector3.zero;
    }

    public void Tick(){
        DoMovement();
    }

    private void DoMovement(){
        float moveSpeed = movementData.walkSpeed;
        Vector2 moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        if (moveInput.sqrMagnitude > 1){
			moveInput.Normalize();
		}
        Vector3 desiredMove = playerTransform.forward * moveInput.y + playerTransform.right * moveInput.x;
        moveDirection.x = desiredMove.x * moveSpeed;
		moveDirection.z = desiredMove.z * moveSpeed;
        charController.Move(moveDirection * Time.fixedDeltaTime);
    }

}