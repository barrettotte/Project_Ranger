using UnityEngine;

public class InputSystem : MonoBehaviour {

    public void ProcessKey(){
        foreach(KeyCode key in System.Enum.GetValues(typeof(KeyCode))){
            if(Input.GetKeyDown(key)){
                switch(key){
                    default:    NoKeybind(key);     break;
                }
                break;
            }
        }
    }

    private void NoKeybind(KeyCode key){
        print(key.ToString() + " is not bound to anything.");
    }

}