# Project_Ranger / Assets /

| **Directory Name** | **Description**                 |
| ------------------ | ------------------------------- |
| [Editor]()         |                                 |
| [Fonts]()          |                                 |
| [Materials]()      |                                 |
| [Resources]()      |                                 |
| [Scenes]()         |                                 |
| [Scripts]()        |                                 |
| [Sprites]()        |                                 |
| [Tests]()          |                                 |