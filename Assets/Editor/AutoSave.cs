﻿using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
 
[InitializeOnLoad]
public class AutoSave{
	//Autosave performed when play button is pressed
    static AutoSave(){
        EditorApplication.playModeStateChanged += (PlayModeStateChange state) => {
            if (EditorApplication.isPlayingOrWillChangePlaymode && !EditorApplication.isPlaying){
                EditorSceneManager.SaveOpenScenes();
                AssetDatabase.SaveAssets();
            }
        };
    }
}