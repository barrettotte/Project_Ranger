var hierarchy =
[
    [ "AutoSave", "class_auto_save.html", null ],
    [ "EditModeExampleTests", "class_edit_mode_example_tests.html", null ],
    [ "MonoBehaviour", null, [
      [ "CameraSystem", "class_camera_system.html", null ],
      [ "GameEventListener", "class_game_event_listener.html", null ],
      [ "GameManager", "class_game_manager.html", null ],
      [ "InputSystem", "class_input_system.html", null ],
      [ "MovementSystem", "class_movement_system.html", null ],
      [ "PlayerManager", "class_player_manager.html", null ]
    ] ],
    [ "PlayModeTests", "class_play_mode_tests.html", null ],
    [ "ScriptableObject", null, [
      [ "CameraData", "class_camera_data.html", null ],
      [ "GameEvent", "class_game_event.html", null ],
      [ "GameManagerData", "class_game_manager_data.html", null ],
      [ "PlayerMovementData", "class_player_movement_data.html", null ]
    ] ]
];