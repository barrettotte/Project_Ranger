var annotated_dup =
[
    [ "AutoSave", "class_auto_save.html", null ],
    [ "CameraData", "class_camera_data.html", "class_camera_data" ],
    [ "CameraSystem", "class_camera_system.html", "class_camera_system" ],
    [ "EditModeExampleTests", "class_edit_mode_example_tests.html", "class_edit_mode_example_tests" ],
    [ "GameEvent", "class_game_event.html", "class_game_event" ],
    [ "GameEventListener", "class_game_event_listener.html", "class_game_event_listener" ],
    [ "GameManager", "class_game_manager.html", null ],
    [ "GameManagerData", "class_game_manager_data.html", null ],
    [ "InputSystem", "class_input_system.html", "class_input_system" ],
    [ "MovementSystem", "class_movement_system.html", "class_movement_system" ],
    [ "PlayerManager", "class_player_manager.html", "class_player_manager" ],
    [ "PlayerMovementData", "class_player_movement_data.html", "class_player_movement_data" ],
    [ "PlayModeTests", "class_play_mode_tests.html", "class_play_mode_tests" ]
];