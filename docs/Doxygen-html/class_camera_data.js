var class_camera_data =
[
    [ "cameraRotationMaxima", "class_camera_data.html#adf0ed9b8adb17049a23dead351a94e7b", null ],
    [ "cameraRotationMinima", "class_camera_data.html#a45c8f94d791f62dbd6844b3f689a3a11", null ],
    [ "cameraSensitivity", "class_camera_data.html#ac98d87d8cadacb62c084d5ee218a6047", null ],
    [ "cameraSmoothFactor", "class_camera_data.html#a6afe4d1a5a4adf4265f10d86d1171ca7", null ],
    [ "useCameraSmooth", "class_camera_data.html#a6216a3ec25b3796f4a7872e3f3bb7c85", null ],
    [ "useVerticalClamp", "class_camera_data.html#a751a08d8652505c1cabd8c92c3153322", null ]
];