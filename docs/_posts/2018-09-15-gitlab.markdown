---
layout: post
title:  "CI for Unity with Gitlab + Docker"
date:   2018-09-15 08:52:20 -0400
---


[![build-pipeline](https://raw.githubusercontent.com/barrettotte/Project_Ranger/master/Files/Documentation/Pipeline-Build.png)](https://raw.githubusercontent.com/barrettotte/Project_Ranger/master/Files/Documentation/Pipeline-Build.png)


## Introduction
This is my first "Dev Blog" with my newly setup GitHub.io page with Jekyll. I may do a small Dev Blog on getting that setup because it was a little odd.
Anyway, I suck at writing a bunch of content, so most of these will be super short and dry.


Since I decided to start fresh after a 6 month hiatus from this project, I figured it would be a good time to learn some new tech.
After starting my first software development job, I realized how important Continous Integration(CI) and unit tests are. I debated on which CI platform to go
with for about a week. 

To summarize my thinking:
* TravisCI - Could not find any resources for building in Unity and probably not beefy enough. 
* Jenkins  - I was close to running a local Jenkins server, but I wanted to learn something newer and shinier. If GitLab didn't work I was going to use this.
* Unity-Cloud - This was the last resort for CI. It obviously supports Unity very well, but I wanted to have the option of in-house hosting.
* GitLab - This was the best case because the cloud CI was available, but I could also host in-house. I obviously decided to use this.

## My GitLab/Docker Adventure

The only reason I got this to work was thanks to this amazing programmer, [Gableroux](https://gitlab.com/gableroux).
Consider donating to his studio's [Patreon](https://www.patreon.com/totemastudio)

### The Good:
* GitLab Free and Bronze plan ($4/month) both have enough features for Unity.
* GitLab is widely used for software development, so the knowledge gained is not limited to just game development.
* The gitlab-ci.yml file allows for flexbile test running, building, and deployment
* Personally, I found the issue board and milestones of GitLab as an amazing project management tool.
* GitLab API is very well documented and allows the creating of various project management scripts.
* GitLab has a Docker Registry, giving it full Docker support.
* GitLab includes a service desk that users can email to auto create issues.
* GitLab supports repository mirroring with GitHub, making it very easy to keep things synced.
* Setting up Unity support in GitLab required Docker, so it forced me to learn another valuable tool.
* Now that another person has run through setting up Unity in GitLab, it might be easier for future people.


### The Bad:
* It was very difficult for me to setup since I had to learn how to use GitLab and Docker.
* Docker does not work very well in Windows, I used an Ubuntu VM to setup my Docker container.
  * Running virtual containers inside of a VM? That's neat
* The Docker container was pretty big on my awful internet, taking me 3 hours for 3.6GB.
* Once my Docker container was setup, I had to upload 7.8GB and it took me an entire day.
* The base pipeline takes about 15 minutes to run through all successful tests/builds.
* The free/bronze plans include 2000 pipeline minutes per month, so obviously do not build on each commit.


### The Ugly:
* It took me about a week and a half to get this setup correctly. I anticipated three days.
* At first, I tried to install Docker on my Windows machine. It completely broke my VirtualBox installation.
  * (CPU is limited to only running one hypervisor - VirtualBox and Docker for Windows use different ones)
  * I waited 3 hours to download the Unity docker image, only to not use it.
* I was constantly failing builds with my Project_Ranger repo.
  * I realized that the Docker container was pointing to the build location echoed twice
    * Could not find folder .../Project_Ranger/.../Project_Ranger/
  * For some reason, it did not like my Unity project being in a separate folder.
  * To fix it I threw all of my Assets, ProjectSettings, etc. into the repo's root folder and it worked.
* Honestly, nothing went too bad. It was just a lot of messing around and waiting for uploads/downloads.


## The Instructions
Using Gableroux's instructions worked okay, but there were a couple things I had trouble understanding since I
was new to this CI world. I documented my whole process of getting everything working correctly in 
[this markdown file](https://github.com/barrettotte/Project_Ranger/blob/master/Files/Notes/GitLab-Unity.md)

I actually used these instructions twice, once for a test project and once for my actual project, Project_Ranger.


## Further Tasks
* I need to figure out a good deployment strategy for my builds.
* I still need to setup a local GitLab server, just for fun and a little experience with server administration.
* Decrease test/build time.
* Start writing some unit tests.


## TLDR;
* Getting this setup is a little janky, but it works well so far.
* [Gableroux](https://gitlab.com/gableroux) is a genius.
* My Unity project with CI hosted [here](https://github.com/barrettotte/Project_Ranger/blob/master/Files/Notes/GitLab-Unity.md)
* Use [these instructions](https://github.com/barrettotte/Project_Ranger/blob/master/Files/Notes/GitLab-Unity.md) to get CI for Unity with GitLab + Docker
