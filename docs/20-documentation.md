---
layout: page
title: Documentation
permalink: /documentation/
---

### [Doxygen-Generated Documentation](../Doxygen-html/index.html)
<br />


<!-- Project Management/Design-->
### Root Project Folder
* [Assets](https://github.com/barrettotte/Project_Ranger/tree/master/Assets) - Unity project assets
* [Files](https://github.com/barrettotte/Project_Ranger/tree/master/Files) - Misc. config files and generated files
* [Scripts](https://github.com/barrettotte/Project_Ranger/tree/master/Scripts) - Various util scripts for generation and convenience
* [docs](https://github.com/barrettotte/Project_Ranger/tree/master/docs) - GitHub.io page
* [.gitlab-ci.yml](https://github.com/barrettotte/Project_Ranger/blob/master/.gitlab-ci.yml) - GitLab-CI configuration
* [Push.bat](https://github.com/barrettotte/Project_Ranger/blob/master/Push.bat) - Main script for committing, generating docs, and triggering pipeline.
<br /><br />

### Software/Technologies
* [Adobe Photoshop]() - Texturing, 2D Art Assets
* [Blender]() - 3D Art Assets
* [Docker]() - Virtual container engine
* [Doxygen]() - Documentation Generation
* [GitHub]() - Source Control
* [GitLab]() - Continous Integration/Deployment
* [Jekyll]() - GitHub.io Page
* [ManicTime]() - Time Tracking
* [Microsoft Visio]() - UML and Diagrams
* [Oracle VM VirtualBox]() - Ubuntu VM for GitHub.io page
* [Unity3D]() - Game Engine
* [Visual Studio Code]() - Coding
* [WakaTime]() - Time Tracking
<br /><br />


### Project Architecture
* TBD
<br /><br />


<!-- Artist -->
### Art Asset Development
* TBD
<br /><br />

### 3D Asset Pipeline Diagram
* TBD
<br /><br />


<!-- DevOps/Web -->
### CI/CD for Unity3D with GitLab + Docker
* Uses a custom Docker image made by [Gableroux](https://github.com/barrettotte/Project_Ranger/blob/master/Files/Notes/GitLab-Unity.md)
* Brief Summary of Installation:
  1. The docker image must be setup once locally for manually activating Unity license
  2. The docker container gets uploaded to GitLab repo's docker registry.
  3. Then, its just like any other GitLab CI environment, driven by the [yaml file](https://github.com/barrettotte/Project_Ranger/blob/master/.gitlab-ci.yml)
  4. [Full Installation Notes](https://github.com/barrettotte/Project_Ranger/blob/master/Files/Notes/GitLab-Unity.md)
* Successfully builds for OSX, Linux, and Windows for Unity 2018.2.6
<br /><br />

### Build Pipeline Diagram
[![build-pipeline](https://raw.githubusercontent.com/barrettotte/Project_Ranger/master/Files/Documentation/Pipeline-Build.png)](https://raw.githubusercontent.com/barrettotte/Project_Ranger/master/Files/Documentation/Pipeline-Build.png)
<br /><br />

### Documentation Generation
* Within [Push.bat](https://github.com/barrettotte/Project_Ranger/blob/master/Push.bat) I added the choice to generate documentation to automate documentation a little better.
* To keep code somewhat documented, [Doxygen](http://www.doxygen.nl/) seemed like a pretty easy tool to use for auto doc generation. (Linked at Top)
* For time tracking I use a combination of [ManicTime](https://www.manictime.com/) and [WakaTime](https://wakatime.com/). 
  * ManicTime is project specific and WakaTime tracks coding time across all projects.
  * [Generate-TimeSheet.py](https://github.com/barrettotte/Project_Ranger/blob/master/Scripts/Generate-TimeSheet.py) searches through ManicTime data
  for relevant project related tasks to generate a Timesheet in Markdown, XML, and CSV files.
* Changelogs are based off of GitLab closed project milestones using [Generate-Changelog.py](https://github.com/barrettotte/Project_Ranger/blob/master/Scripts/Generate-ChangeLog.py) to generate Markdown and XML files.
* The root project [README.md](https://github.com/barrettotte/Project_Ranger/blob/master/README.md) is generated with [Generate-README.py](https://github.com/barrettotte/Project_Ranger/blob/master/Scripts/Generate-README.py) using generated changelog and timesheet.
<br /><br />

### GitHub.io Page with Jekyll
* Page source located in [docs/](https://github.com/barrettotte/Project_Ranger/tree/master/docs)
* Since [Jekyll](https://jekyllrb.com/) support is built straight into GitHub pages, its a no-brainer to use it for a simple project page no one is ever going to look at.
* Also adds a super simple blogging feature that is perfect for the occasional small DevBlog.