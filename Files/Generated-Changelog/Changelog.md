# Project Ranger
<pre>
 <h3>V0.05 - Deciding on new Architecture/Design Pattern and GitHub.io Page Content</h3>
   01.   Make the big decision on the new architecture/design pattern to implement
   02.   Link Doxygen documentation to GitHub.io page's documentation
   03.   Add XML export to timesheet and changelog generator
   04.   Make build artifacts from last successful pipeline downloadable and link it on repo
   05.   Create CI/CD pipeline diagram and add to documentation
   06.   Write Dev Blog for Unity CI Setup
   07.   Repair Github.io page and design dev blog posts
   08.   Remove Gem theme dependency from GitHub.io page
   09.   Create script for generating basic README file including the change log, time sheet, etc
   10.   Create backup script for zipping project and saving to specified path
   11.   Add markdown export for TimeSheet script for Totals
   12.   Add home page content to Github.io page
   13.   Add content to changelog page
   14.   Add to documentation page: Workflow, relevant information, etc
   15.   Populate Github.io page with relevant information about the project
   16.   Start redesigning project for new design pattern
   Start: 2018-09-12  ---  End: 2018-09-19
</pre>

<pre>
 <h3>V0.04 - GitLab CI/CD and Docker Implementation</h3>
   01.   Add generator scripts to push script for better automation
   02.   Get Unity docker image working correctly with GitLab
   03.   Create or find a suitable Unity Docker image
   04.   Install Docker on Ubuntu VM and learn the basics
   05.   Create a shell script for pushing to multiple repos in Ubuntu environment
   06.   Add start/end dates on changelog
   07.   Install and configure Photoshop
   08.   Get drawing tablet software installed and configured
   09.   Learn how to use the Job system for safe multithreading
   10.   Setup asset cache server
   11.   Setup Automated builds on a schedule
   12.   Add Doxygen documentation generation to push script
   13.   Implement Doxygen for simple code reference
   14.   Learn the basics of GitLab
   15.   Implement scheduled builds and testing in GitLab
   16.   Add simple CI/CD with GitLab
   Start: 2018-09-03  ---  End: 2018-09-12
</pre>

<pre>
 <h3>V0.03 - DevOps Foundations</h3>
   01.   Ensure service desk and/or issue submits are working correctly
   02.   Add an auto-save script to the editor to lose less time when a crash occurs
   03.   Research how to test cross platform support through DevOps
   04.   Setup Github.io dev dependencies on Ubuntu VM - Jekyll, Ruby
   05.   Setup Ubuntu 18 VM for Misc. development
   06.   Figure out how to keep GitHub and GitLab repos synced
   07.   Organize issues based off of job role
   08.   Create a script for generating a TimeSheet from ManicTime data
   09.   Update format of README and organize project a little better
   10.   Refresh on project after 6 month long hiatus
   11.   Create script to generate changelog based off of GitLab issues
   12.   Create basic Github.IO page skeleton for this project
   13.   Finish putting together development server
   14.   Upgrade Unity and Blender to latest versions
   15.   Switch project over to VS Code and configure new workspace
   16.   Redesign changelog process and make issue boards
   Start: 2018-08-30  ---  End: 2018-09-03
</pre>

<pre>
 <h3>V0.02 - Item Drop Additions and MORE Inventory Polishing</h3>
   01.   Stats UI design
   02.   Consumable items can be consumed
   03.   Version number and title in HUD
   04.   Textbox to type in split amount
   05.   Equipment UI design
   06.   Open quickbar slots are filled first before inventory is
   07.   Prompt UI updated for piles of items
   08.   Code refactoring to cache more and search less, better performance with inventory
   09.   Storage inventories load with default sizes
   10.   Fixed two bugs with inventory slot colors not working correctly
   11.   Pressing 'SHIFT' and clicking drop will drop the whole stack
   12.   Extra items in an item pile will be redropped if inventory is full
   13.   Dropped items have proper colliders and will fall from player
   14.   Items can be dropped and picked up in item piles
   15.   Items stack onto previously made stacks in available slots
   16.   Default model and sprite if there is an error loading item's assets
   Start: 2018-03-14  ---  End: 2018-03-20
</pre>

<pre>
 <h3>V0.01 - Inventory Polishing and Storage Inventories</h3>
   01.   Items are put back in storage/inventory if inventory closed while moving an item stack
   02.   Movement restricted when accessing storage
   03.   Multiple storage inventory management
   04.   Split stack functionality within storage inventory
   05.   Item stack movement between inventory and storage
   06.   Storage inventory UI
   07.   Storage object implementation
   08.   Item stack of different size can be swapped with stacks of the same item
   09.   Item quality and item rarity added to item system
   10.   Focused item actions UI and functionality
   11.   Focused item UI stays up if there are still items that can be split
   12.   Focused item updates values when stack is split
   13.   Split stack buttons to increment/decrement split by one
   14.   Split stack leftovers remain in hand until placed in available slot
   15.   Dropping items and stacks of items from inventory
   16.   Adding stacks of items to other stacks
   Start: 2018-03-11  ---  End: 2018-03-14
</pre>

<pre>
 <h3>V0.00 - Player Setup and Inventory Foundations</h3>
   01.   Item split stack
   02.   Focused item UI
   03.   Inventory slot movement and swapping
   04.   Inventory hover icon when selecting a slot
   05.   Item system using XML Serialization
   06.   Inventory size can change dynamically
   07.   Camera locks when in inventory
   08.   Raycasting system for object interaction
   09.   Inventory togglable with 'TAB'
   10.   Crosshair and prompt UI added
   11.   Items have max stack sizes
   12.   Items can be added to inventory
   13.   Interactable and item object implementation
   14.   Inventory UI basics
   15.   First person camera controls
   16.   Basic movement (walking, running, jumping)
   Start: 2018-03-07  ---  End: 2018-03-11
</pre>

