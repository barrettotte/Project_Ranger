<pre>
 <h3>V0.05 - Deciding on new Architecture/Design Pattern and GitHub.io Page Content</h3>
   01.   Make the big decision on the new architecture/design pattern to implement
   02.   Link Doxygen documentation to GitHub.io page's documentation
   03.   Add XML export to timesheet and changelog generator
   04.   Make build artifacts from last successful pipeline downloadable and link it on repo
   05.   Create CI/CD pipeline diagram and add to documentation
   06.   Write Dev Blog for Unity CI Setup
   07.   Repair Github.io page and design dev blog posts
   08.   Remove Gem theme dependency from GitHub.io page
   09.   Create script for generating basic README file including the change log, time sheet, etc
   10.   Create backup script for zipping project and saving to specified path
   11.   Add markdown export for TimeSheet script for Totals
   12.   Add home page content to Github.io page
   13.   Add content to changelog page
   14.   Add to documentation page: Workflow, relevant information, etc
   15.   Populate Github.io page with relevant information about the project
   16.   Start redesigning project for new design pattern
   Start: 2018-09-12  ---  End: 2018-09-19
</pre>

