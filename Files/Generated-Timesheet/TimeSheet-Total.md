## Project Ranger Time Sheet

#### 2018-08-30 - 2018-09-23
| **Application** | **Time**            |
| --------------- | ------------------- |
| Total Google Chrome  | 06:04:04      |
| Total Oracle VM VirtualBox  | 13:12:20      |
| Total Blender  | 00:02:49      |
| Total Adobe Photoshop CS6  | 00:15:57      |
| Total Unity  | 05:22:17      |
| Total Visual Studio Code  | 13:56:07      |
| Total Microsoft Visio  | 01:02:17      |
|      Total      | **39:55:51**  |
