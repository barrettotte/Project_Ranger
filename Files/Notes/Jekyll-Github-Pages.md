# Jekyll + Gitub.io Page

## Installation
* ```$ sudo apt-get -y install zlib1g-dev```
* ```$ sudo apt-get -y install ruby-full```
* ```$ gem install jekyll```

## New Project
* ```$ jekyll new page-name```
* ```$ cd page-name```

## Gemfile Setup
Make sure these are included in Gemfile
```
source "https://rubygems.org"
gem "github-pages", group: :jekyll_plugins
```
```$ bundle update```

## Running Locally
* ```$ bundle exec jekyll serve --watch```
* Running locally at http://localhost:4000

## Removing Gem Theme Dependance
* Run ```$ bundle show minima```
* Copy files in gem folder (ex: /var/lib/gems/2.5.0/gems/minima-2.5.0) to .io page's directory
* Copy Gemspec plugin dependencies to .io page's Gemfile
* ```$ bundle update```

## Github Push
* Make sure to follow instructions in Gemfile for uncommenting ```gem "github-pages"...```
* Add, commit, push repo
* In GitHub repo settings, select **master/docs**
* Clicking on Commits, you can see the build progress of the .io page next to each commit

## Sources
* https://jekyllrb.com/docs/github-pages/
* https://jekyllrb.com/docs/themes/#overriding-theme-defaults