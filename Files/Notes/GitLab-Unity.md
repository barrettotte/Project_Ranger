# GitLab + Docker + Unity Notes


Implementing CI/CD to Unity Project using Docker and GitLab CI.

Before starting up my game development project, I decided to learn some basic DevOps
with GitLab. This soon led me to learning a little bit of Docker in order for automatic
builds to work. Overall I think I would have saved a lot of time using Unity's cloud build
feature, but this gave me a little experience in very valuable DevOps tools.

**Gableroux** (sources below) is a lifesaver, otherwise I would have given up a lot sooner.


## Sources
* GitLab CI Unity Example https://gitlab.com/gableroux/unity3d-gitlab-ci-example
* Unity3D Docker Image 
  * https://gitlab.com/gableroux/unity3d
  * https://hub.docker.com/r/gableroux/unity3d/


## Local Setup

Before pushing Docker image to GitLab it must be built locally

1. Install Docker to Ubuntu VM - [Source](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04)
  * ```sudo apt update```
  * ```sudo apt install apt-transport-https ca-certificates curl software-properties-common```
  * ```curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -```
  * ```sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"```
  * ```sudo apt update```
  * ```sudo apt install docker-ce```
2. Pull base Docker Image (By: **Gableroux**) - [Source](https://hub.docker.com/r/gableroux/unity3d/)
  * ```docker pull gableroux/unity3d```
3. Create/use included Dockerfile
4. Build GitLab Docker image
  * ```docker login```
  * ```docker login registry.gitlab.com```
  * ```docker build -t registry.gitlab.com/username/project .```
5. Verify image is built
  * ```docker images``` 
6. Run **Unity-Docker-Setup.sh** (By: **Gableroux**)
  * ```chmod u+x ./Unity-Docker-Setup.sh && sudo ./Unity-Docker-Setup.sh```
  * This script will run the container and setup its environment variables.
  * The credentials will need to be manually filled in since I removed my own.
7. Inside container run (By: **Gableroux**)
  * 
``` bash
xvfb-run --auto-servernum --server-args='-screen 0 640x480x24' opt/Unity/Editor/Unity -logFile -batchmode -username "$UNITY_USERNAME" -password "$UNITY_PASSWORD"
```
8. Save xml output (LICENSE SYSTEM [####] Posting ...) to unity3d.alf
9. Open https://license.unity3d.com/manual and answer prompts.
10. Download Unity_v###.ulf and copy XML content to CI's environment variables.
  * GitLab Project > Settings > CI/CD > Variables > Save as **UNITY_LICENSE_CONTENT**
11. Check if **gitlab-ci.yml** in project directory is configured correctly with build targets, names, etc.
12. Remove credentials from Unity-Docker-Setup.sh 
13. Push project to existing repo
  * ```git add .```
  * ```git commit -m "Added "```
  * ```git push origin master```
14. Push container to GitLab Registry or DockerHub Repository
  * ```docker login```
  * ```docker login registry.gitlab.com```
  * ```docker push registry.gitlab.com/username/project```
