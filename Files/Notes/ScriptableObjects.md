# Game Architecture with Scriptable Objects Talk 01
**Source:** https://www.youtube.com/watch?v=raQ3iHhE_Kk


## Engineering Pillars

### Modular
* Systems are not directly dependent on each other. 
  * For example, an inventory system should be able to communicate with other systems, but not be hardwired.
* Scenes are clean slates
  * No transient data between scenes, minimizes dontDestroyOnLoad objects. 
  * A scene is a clean load, a clean break.
* Prefabs work on their own.
  * Have the prefabs functionality contained inside itself, modular.
* Components!
  * Break things up into components that do one thing and one thing only.

### Editable
* Focus on Data
  * Data driven -> make systems into machines that just process data as instructions.
* Change the game without code
  * Being component-based/modular makes it flexible to edit.
  * Options can be easily changed without changing code.
* Emergent Design
  * Tiny components that are great at their one job can be combined together to form a new feature.
* Change at runtime
  * The more the game can be changed at runtime, the more the game can be balanced.
  * Saving a runtime state back out can be valuable for testing, debugging, balancing. 

### Debuggable
* Test in isolation (modularity)
  * The more modular the game is, the easier it is to test.
* Debug views and features
  * The more editor features, the more editing can be done.
  * Never plan a feature as done until the method of debugging is planned.
* Never fix a bug you do not understand.


## Scriptable Object

### Basics
* Serializable Unity Class
* Similar to MonoBehavior, but no Transform and no GameObject.
* MonoBehavior and Scripatable Object are the same exact type on C++ engine layer.
* Saved as .asset file
  * Not necessary, can be left in memory and work with them there at runtime.
  * Can save more than one Scriptable Object in a .asset file.

### Simple Use Cases
* Game config files
* Inventory
* Enemy Stats
* AudioCollection


## Architecture

### Singleton Benefits
* Access anything from anywhere
* Persistent State
* Easy to understand
* Consistent pattern
* Easy to "plan"

### Singleon Problems
* Rigid Connections / Not Modular
* No Polymorphism
* Not testable
* Dependency Nightmare
* Single Instance!

## Solutions
* Reduce need for global managers
* Inversion of Control
  * Example: Dependency Injection
  * Objects are given dependencies instead of fetching them
  * Single responsibility principle
  

## Modular Data

### Enemy Prefab Example
* EnemyManager.Instance.MoveSpeed ? 
  * Hard reference
  * Dependency on manager being loaded
* EnemyConfig ScriptableObject reference?
  * Lumps all part of the enemy stats together
  * Limitss extension / modularity

### ScriptableObject Variables
```C#
[CreateAssetMenu]
public class FloatVariable : ScriptableObject{
    public float Value;
}
```
* Hitpoints
* Damage amounts
* Timing data

### Float Reference
```C#
[Serializable]
public class FloatReference{
    public bool UseConstant = true;
    public float ConstantValue;
    public FloatVariable Variable;
    public float Value{
        get{ return UseConstant ? ConstantValue : Variable.Value; }
    }
}
```
```C#
public class DumbEnemy : MonoBehavior{
    public FloatReference MaxHP;
    public FloatReference MoveSpeed;
}
```

### Float Variable Secrets
```C#
[CreateAssetMenu]
public class FloatVariable : ScriptableObject{
    public float Value;
}
```

## Event Architecture
* Modularize systems
  * Reuse in other projects 
  * Isolates Prefabs
* Optimize, only execute when needed
* Highly Debuggable

### UnityEvent
* Editor hook-ups
* Less code, fewer assumptions
* Pass arguments
* Rigid bindings
* Limited serialization
* Garbage allocation

### Making our own events
* Data driven
* Designer authorable
* Debuggable
```C#
[CreateAssetMenu]
public class GameEvent : ScriptableObject{
    private List<GameEventListener> listeners = new List<GameEventListener>();
    public void Raise(){
        for(int i = listeners.Count - 1; i >= 0; i--){
            listeners[i].OnEventRaised();
        }
    }
    public void RegisterListener(GameEventListener listener)...
    public void UnregisterListener(GameEventListener listener)...
}

public class GameEventListener : MonoBehavior{
    public GameEvent Event;
    public UnityEvent Response;
    private void OnEnable(){
        Event.RegisterListener(this);
    }
    private void OnDisable(){
        Event.UnregisterListener(this);
    }
    public void OnEventRaised(){
        Response.Invoke();
    }
}
```

## Scriptable Objects vs...

### Singleton Manager goals/problems
* Track all enemies in a scene
* Understand player
* Issue commands
* Race conditions
* Rigid singleton
* Only One

### Runtime Sets
* Keep track of a list of objects in the scene
* Avoid race conditions
* More flexible than Unity tags, singleton
```C#
public abstract class RuntimeSet<T> : ScriptableObject{
    public List<T> Items = new List<T>();
    public void Add(T t){}
    public void Remove(T t){}
}
```
* Buildings placed in an RTS
* Renderers for special effects
* Items on a map

### Enums
* Must change in code
* Difficult to remove / reorder
* Cant hold additional data


## Asset Based Systems

### Generic Systems
* System is ScriptableObject Asset in project
* Reference directly with inspector
* No code lookup
* No scene-only references

### Inventory
* ScriptableObject Master List
* ScriptableObject per item
* Use different Inventories in different scenes





# Game Architecture with Scriptable Objects Talk 02
**Source:** https://www.youtube.com/watch?v=6vmRwLYWNRo