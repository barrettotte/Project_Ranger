# Entity Component System (ECS)
**Source:** https://unity3d.com/learn/tutorials/topics/scripting/introduction-ecs?playlist=17117



## Introduction to ECS and Job System

### The "Classic" Way
* Monobehaviors
* Data / Processing Coupled
* Largely dependant on reference types
* Problems
  * Data is scattered
  * Loading from memory to cache is very slow
  * A bunch of extra data is processed
  * Processing is done one at a time...Mostly on one thread

### Job System Overview
* Separate data from function
* Multi-core processing
* Save multi-threading
* Job system benefits
  * Average core count in CPUs is increasing
  * Most cores go unused  
* Multithreading problems
  * Thread safe code is difficult
  * Race conditions
  * Context switching is expensive  
* Solving Multithreading problems with Job system
  * Job system manages this for you
  * Focus entirely on game specific code instead

### Entity Component System
* Benefits
  * Performance
  * Easier to write highly optimized code
  * Easier to write reusable code
  * Leverage modern hardware architecture
  * Archetypes are tightly packed in memory
* ECS and Job System work very well together
*   