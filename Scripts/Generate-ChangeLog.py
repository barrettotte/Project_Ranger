# Generate-Changelog.py
# Barrett Otte
#
# This script will grab all milestones with all their issues as JSON.
# Then parse the changelog JSON to create a markdown and xml file
# 
# NOTES:
#    - Docs https://docs.gitlab.com/ee/README.html
#    - When/if the milestones count gets to 20, pagination will need to be implemented.
#    - The config could go in a separate file, but this is such a simple script, why
#        not just keep everything in a single file?


import urllib3, json, io, os

# ======================== CONFIG ========================
baseUrl = "https://gitlab.com/api/v4/"
accessToken = ""
projectId = ""
privateConfigFile = "_Barrett-Private/private.json"
changelogTitle = "Project Ranger"
outputPath = "Files/Generated-Changelog/"
fileName = "Changelog"
showOpen = False
generateMdChangelog = True
generateXmlChangelog = True
generateGithubIOChangelog = True
tab = '    '
githubIOHeader = ["---", "layout: page", "title: Changelog", "permalink: /changelog/", "---"]
githubIOFile = "docs/05-changelog.md"
# ========================================================


def getPrettyJson(jsonRaw):
    return json.dumps(jsonRaw, indent=4, sort_keys=True)

def rptStr(s, i):
    return s * i

def xmlMilestone(issuesJson, milestone):
    xmlBuffer = tab + "<milestone>\n" + rptStr(tab,2) + "<title>" + milestone["title"] + "</title>\n"
    for j in range(len(issuesJson)):
        xmlBuffer += rptStr(tab,2) + "<issue>\n"
        issue = issuesJson[j]
        xmlBuffer += rptStr(tab,3) + "<name>" + str(issue["title"]) + "</name>\n" + rptStr(tab,2) + "</issue>\n"
    xmlBuffer += rptStr(tab,2) + "<timeframe>\n" + rptStr(tab,3) + "<startDate>" + str(milestone["start_date"]) + "</startDate>\n"
    xmlBuffer += rptStr(tab,3) + "<endDate>" + str(milestone["due_date"]) + "</endDate>\n" + rptStr(tab,2) + "</timeframe>\n"
    xmlBuffer += tab + "</milestone>\n"
    return xmlBuffer

def mdMilestone(issuesJson, milestone):
    mdBuffer = "<pre>\n <h3>" + milestone["title"] + "</h3>\n"
    for j in range(len(issuesJson)):
        issue = issuesJson[j]
        mdBuffer += "   " + str(j+1).zfill(2) + ".   " + str(issue["title"]) + "\n"
    mdBuffer += "   Start: " + str(milestone["start_date"]) +  "  ---  End: " + str(milestone["due_date"])
    return mdBuffer + "\n</pre>\n"

def writeChangelog(buffer, name):
    with open(name, 'w') as f:
        f.write(buffer)
        print("Successfully created " + name)

def main():
    wroteLatest = False
    if not os.path.exists(outputPath):
        print ("Changelog directory not found. Creating directory at path: " + outputPath)
        os.makedirs(outputPath)

    with open(privateConfigFile, 'r') as privateConfig:
        privateData = json.load(privateConfig)
        accessToken = privateData["gitlabAccessToken"]
        projectId = privateData["gitlabProjectId"]

    urllib3.disable_warnings()
    http = urllib3.PoolManager()
    print("Creating file " + fileName + ".md")
    milestonesUrl = baseUrl + "projects/" + projectId + "/milestones"
    milestones = http.request("GET", milestonesUrl, headers = {"PRIVATE-TOKEN": accessToken})
    milestonesJson = json.loads(milestones.data.decode("utf-8"))
    print("Found " + str(len(milestonesJson)) + " milestone(s) to write.")

    if generateMdChangelog:
        mdBuffer = "# " + changelogTitle + "\n"
    if generateXmlChangelog:
        xmlBuffer = '<?xml version="1.0" encoding="UTF-8"?>\n<changelog>\n'
    for i in range(len(milestonesJson)):
        milestone = milestonesJson[i]
        if milestone["state"] == "closed" or showOpen == True:
            print("   Writing milestone   " + milestone["title"])
            issues = http.request("GET", milestonesUrl + "/" + str(milestone["id"]) + "/issues", headers = {"PRIVATE-TOKEN": accessToken})
            issuesJson = json.loads(issues.data.decode("utf-8"))
            if generateMdChangelog:
                milestoneMd = mdMilestone(issuesJson, milestone) + "\n"
                mdBuffer += milestoneMd
                if not wroteLatest:
                    writeChangelog(milestoneMd, outputPath + fileName + "-latest.md")
                    wroteLatest = True
            if generateXmlChangelog:
                xmlBuffer += xmlMilestone(issuesJson, milestone)

#   writeChangelog(getPrettyJson(milestonesJson), outputPath + fileName + ".json")
    if generateMdChangelog:
        writeChangelog(mdBuffer, outputPath + fileName + ".md")
    if generateXmlChangelog:
        writeChangelog(xmlBuffer + "</changelog>", outputPath + fileName + ".xml")
    if generateGithubIOChangelog:
        ghiBuffer = ""
        for line in githubIOHeader:
            ghiBuffer += line + "\n"
        ghiBuffer += mdBuffer
        writeChangelog(ghiBuffer, githubIOFile)


if __name__ == "__main__":main()