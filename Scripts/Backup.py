# Backup.py
# Barrett Otte
#
# This script will create a zip file and store it in
#   configured backup directory with user defined name.
# 
# NOTES:
#  - Ignoring only works on project root folders, no subdirectories.
#    ex: Will ignore     "~\_privateFolder\" 
#        Will not ignore "~\_assets\privateFolder\"

import os, zipfile

# ======================== CONFIG ========================
projectAbsPath = "D:\\Game Development\\Project_Ranger"
backupDirIgnore = ["_Barrett-Private"]
backupStorageAbsPath = "D:\\Game Development\\Project_Ranger\\_Barrett-Private\\Backups\\"
backupBaseName = "Project_Ranger"
# ========================================================
    

def main():
    backupCount = 0
    if not os.path.exists(backupStorageAbsPath):
        print ("Backup directory not found. Creating directory at path: " + backupStorageAbsPath)
        os.makedirs(backupStorageAbsPath)
    else:
        for files in os.listdir(backupStorageAbsPath):
            if files.endswith('.zip'):
                backupCount += 1
        print ("Total number of backup files: " + str(backupCount))
    backupZipName = backupBaseName + "-" + str(backupCount).zfill(3) + ".zip"
    print("Saving backup as " + backupStorageAbsPath + backupZipName)
    zipf = zipfile.ZipFile(backupStorageAbsPath + backupZipName, 'w', zipfile.ZIP_DEFLATED)

    for dirName, subdirs, files in os.walk(projectAbsPath):
        ignore = dirName.split(projectAbsPath)[1].split("\\")
        if len(ignore) > 1:
            ignore = ignore[1]
        if not ignore in backupDirIgnore:
            for fileName in files:
                absName = os.path.abspath(os.path.join(dirName, fileName))
                arcName = absName[len(projectAbsPath) + 1:]
                zipf.write(absName, arcName)
    zipf.close()


if __name__ == "__main__":main()