# Generate-TimeSheet.py
# Barrett Otte
#
# This script will generate a timesheet based off of ManicTime data and specified
#    targets manually set by the user.
#
# I know something similar can be achieved through ManicTime's tag system, but
#    it seemed a little janky and I like making homemade tools.
#
# Its certainly not ideal that this generates the entire timesheet for the range
#    defined in the batch file, but I think it will work for the scope of this project.
#
# The processTargets list is used to defined targets to include in the timesheet.
#   - The first element is the application name
#   - The second element is the specified process name for the application
#        For example, the Google Chrome target will only include processes with the 3
#           specified names in its underlying list.
#        As expected, if the underlying list is empty, then all processes of the application
#           will be counted in the timesheet.
#   - The third element is the time spent in seconds for a particular date. Just a place
#        where I can store temp data, but it can also be used to add a daily offset to an application.
#   - The fourth element is for the total time in seconds for the entire date range, again 
#        another place to store temp data and an offset can be added.
#
# Dependencies:
#    python-dateutil
#


import csv, sys, os
from dateutil.parser import parse


# ======================== CONFIG ========================
processTargets = [
    ["Google Chrome", 
        [
            "Boards · Barrett Otte / Project_Ranger · GitLab - Google Chrome",
            "Milestones · Barrett Otte / Project_Ranger · GitLab - Google Chrome",
            "Barrett Otte / Project_Ranger · GitLab - Google Chrome",
            "barrettotte/Project_Ranger: My main game development project, named after my cat. - Google Chrome"
        ], 
        0, 0
    ],
    ["Oracle VM VirtualBox", ["Ubuntu18-Dev [Running] - Oracle VM VirtualBox : 1"], 0, 0],
    ["Blender", [], 0, 0],
    ["Adobe Photoshop CS6", [], 0, 0],
    ["Unity", [], 0, 0],
    ["Visual Studio Code", ["Project_Ranger"], 0, 0],
    ["Microsoft Visio", [], 0, 0]
]
timesheetName = "TimeSheet"
timesheetTitle = "Project Ranger Time Sheet"
user = "Barrett Otte"
outputPath = "Files/Generated-Timesheet/"
tab = '    '
# ========================================================

def rptStr(s, i):
    return s * i

def makeTimeString(seconds):
    totalSecs, sec = divmod(seconds, 60)
    hour, minute = divmod(totalSecs, 60)
    return str(hour).zfill(2) + ":" + str(minute).zfill(2) + ":" + str(sec).zfill(2)

def formatDateStr(dateStr):
    dateStr = dateStr.split(" ")[0].replace("/", "-").split("-")
    return "".join([dateStr[2]+"-", dateStr[0].zfill(2)+"-", dateStr[1].zfill(2)])

def getAppTimeSecs(timeString):
    timeSplit = timeString.split(":")
    return (int(timeSplit[2]) + (int(timeSplit[1]) * 60) + (int(timeSplit[0]) * 3600))
    
def isDate(s):
    try:
        parse(s)
        return True
    except ValueError:
        return False

def appendAppTime(timesheetName, dateIter):
    totalTime = 0
    print("  Appending time data for " + dateIter + " to " + timesheetName + ".csv")
    with open(outputPath + timesheetName + ".csv", "a", newline='') as timesheet:
        writer = csv.writer(timesheet, delimiter=",")       
        writer.writerow(["Date", dateIter])
        for application in processTargets:
            writer.writerow(["", application[0], makeTimeString(application[2])])
            totalTime += application[2]
            application[2] = 0
        writer.writerow(["Total", "", makeTimeString(totalTime)])
        writer.writerow([])

def csvHeaderToMd(row):
    buffer = ""
    buffer += "\n#### " + row[1] + "\n"
    buffer += "| **Application** | **Time**            |\n"
    buffer += "| --------------- | ------------------- |\n"
    return buffer
    
def timesheetToMarkdown(csvName, days):
    mdBuffer = ""
    print("Writing " + csvName + "-Total.md")
    with open(csvName + ".csv", "r", encoding="utf8") as timesheetCsv:
        csvReader = csv.reader(timesheetCsv, delimiter=",")
        titleRow = next(csvReader)
        mdBuffer = ""
        currentDay = 0
        for row in csvReader:
            if len(row) > 0:
                if currentDay == days:
                    currentDay += 1
                    mdBuffer = "## " + titleRow[0] + "\n"
                if row[0] == "Date":
                    mdBuffer += csvHeaderToMd(row)
                elif row[0] == "Total":
                    mdBuffer += "|      Total      | **" + row[2] + "**  |\n"
                    if currentDay < days:
                        currentDay += 1
                else:
                    mdBuffer += "| " + row[1] + "  | " + row[2] + "      |\n"
    with open(csvName + "-Total.md", "w", encoding="utf8") as totalTimeMd:
        totalTimeMd.write(mdBuffer) 

def timesheetToXml(csvName):
    xmlBuffer = '<?xml version="1.0" encoding="UTF-8"?>\n<timesheet>'
    print("Writing " + csvName + ".xml")
    with open(csvName + ".csv", "r", encoding="utf8") as timesheetCsv:
        csvReader = csv.reader(timesheetCsv, delimiter=",")
        titleRow = next(csvReader)
        xmlBuffer += "\n" + tab + "<title>" + titleRow[0] + " - " + titleRow[1] + "</title>\n"
        for row in csvReader:
            if len(row) > 0:
                if row[0] == "Date":
                    xmlBuffer += tab + "<day>\n" + rptStr(tab,2) + "<value>" + row[1] + "</value>\n"
                elif row[0] == "Total":
                    xmlBuffer += rptStr(tab,2) + "<total>" + row[2] + "</total>\n" + tab + "</day>\n"
                else:
                    xmlBuffer += rptStr(tab,2) + "<application>\n" + rptStr(tab,3) + "<value>" + row[1] + "</value>\n"
                    xmlBuffer += rptStr(tab,3) + "<time>" + row[2] + "</time>\n" + rptStr(tab,2) + "</application>\n"
        xmlBuffer += "</timesheet>"
    with open(csvName + ".xml", "w", encoding="utf8") as timesheetXml:
        timesheetXml.write(xmlBuffer)
        
def main():
    dates = []
    mtFileName = sys.argv[1]
    totalTime = 0
    if not os.path.exists(outputPath):
        print ("Timesheet directory not found. Creating directory at path: " + outputPath)
        os.makedirs(outputPath)
    print("Reading " + mtFileName)
    with open(outputPath + timesheetName + ".csv", "w", newline='') as timesheet:
        writer = csv.writer(timesheet, delimiter=",")
        writer.writerow([timesheetTitle, user])
        writer.writerow([])
    with open(mtFileName, "r", encoding="utf8") as mtFile:
        mtReader = csv.reader(mtFile, delimiter=",")
        next(mtReader)
        for row in mtReader:
            for application in processTargets:
                if row[4] == application[0]:
                    if len(application[1]) > 0:
                        for process in application[1]:
                            if application[0] == "Visual Studio Code" and application[1][0] in row[0]:
                                    appTime = getAppTimeSecs(row[3])
                                    application[2] += appTime
                                    application[3] += appTime
                            else:
                                if row[0] == process:
                                    appTime = getAppTimeSecs(row[3])
                                    application[2] += appTime
                                    application[3] += appTime
                    else:
                        appTime = getAppTimeSecs(row[3])
                        application[2] += appTime
                        application[3] += appTime
            tmp = formatDateStr(row[2])
            if isDate(tmp) and tmp not in set(dates):
                if len(dates) > 0:
                    appendAppTime(timesheetName, dates[-1])
                dates.append(tmp)
    appendAppTime(timesheetName, dates[-1])
    print("Writing " + outputPath + timesheetName + ".csv")
    with open(outputPath + timesheetName + ".csv", "a", newline='') as timesheet:
        writer = csv.writer(timesheet, delimiter=",")
        writer.writerow(["Date", dates[0] + " - " + dates[-1]])
        for application in processTargets:
            writer.writerow(["", "Total " + application[0], makeTimeString(application[3])])
            totalTime += application[3]
        writer.writerow(["Total", "", makeTimeString(totalTime)])
        writer.writerow([])   
    timesheetToMarkdown(outputPath + timesheetName, len(dates))
    timesheetToXml(outputPath + timesheetName)
                
if __name__ == "__main__": main()