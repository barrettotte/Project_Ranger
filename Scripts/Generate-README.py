# Generate-README.py
# Barrett Otte
#
# Generate project README.md from other generated markdown
# Badges: https://shields.io/#/

import urllib3, json

# ======================== CONFIG ========================
user = "barrettotte"
gitHubRepo = "https://github.com/barrettotte/Project_Ranger/"
gitLabRepo = "https://gitlab.com/barrettotte/Project_Ranger/"
gitLabIssueBoard = "https://gitlab.com/barrettotte/Project_Ranger/boards/"
gitLabPipelines = "https://gitlab.com/barrettotte/Project_Ranger/pipelines"
gitLabBranch = "master"
changelogPath = "Files/Generated-Changelog/Changelog-latest.md"
timesheetPath = "Files/Generated-Timesheet/TimeSheet-Total.md"
screenshotUrl = "https://github.com/barrettotte/Project_Ranger/blob/master/Files/Screenshots/latest.png"
projectTitle = "Project_Ranger"
projectDesc = "My main game development project, named after my cat."

gitLabAPIUrl = "https://gitlab.com/api/v4/"
accessToken = ""
projectId = ""
privateConfigFile = "_Barrett-Private/private.json"

linkList = [
    ["GitHub.io Page", "https://barrettotte.github.io/Project_Ranger/"],
    ["GitHub Repo", "https://github.com/barrettotte/Project_Ranger/"],
    ["GitLab Repo", "https://gitlab.com/barrettotte/Project_Ranger/"],
    ["Issue Board", "https://gitlab.com/barrettotte/Project_Ranger/boards/"],
    ["DockerHub Repo", "https://hub.docker.com/u/barrettotte/"],
    ["Imgur Album", "https://imgur.com/a/xGWNicy"],
    ["ArtStation Page", "https://www.artstation.com/barrettotte"],
    ["WakaTime Stats", "https://wakatime.com/@barrettotte"]
]
# ========================================================

def writeBuffer(buffer, name):
    with open(name, 'w') as f:
        f.write(buffer)
        print("Successfully created " + name)

def getPrettyJson(jsonRaw):
    return json.dumps(jsonRaw, indent=4, sort_keys=True)

def getBuildLinks():
    buildLinks = [
        ["Linux", "", "build-StandaloneLinux64"],
        ["OSX", "", "build-StandaloneOSX"],
        ["Windows", "", "build-StandaloneWindows64"]
    ]
    with open(privateConfigFile, 'r') as privateConfig:
        privateData = json.load(privateConfig)
        accessToken = privateData["gitlabAccessToken"]
        projectId = privateData["gitlabProjectId"]
    urllib3.disable_warnings()
    http = urllib3.PoolManager()
    pipelinesUrl = gitLabAPIUrl + "projects/" + projectId + "/pipelines"
    pipelines = http.request("GET", pipelinesUrl, headers = {"PRIVATE-TOKEN": accessToken})
    pipelinesJson = json.loads(pipelines.data.decode("utf-8"))
    for i in range(len(pipelinesJson)):
        pipeline = pipelinesJson[i]
        if pipeline["status"] == "success":
            jobsUrl = gitLabAPIUrl + "projects/" + projectId + "/pipelines/" + str(pipeline["id"]) + "/jobs"
            jobs = http.request("GET", jobsUrl, headers = {"PRIVATE-TOKEN": accessToken})
            jobsJson = json.loads(jobs.data.decode("utf-8"))
            for j in range(len(jobsJson)):
                job = jobsJson[j]
                if job["status"] == "success" and job["stage"] == "build":
                    jobName = job["name"]
                    artifactsUrl = job["web_url"] + "/artifacts/browse/Builds/"
                    for k in range(len(buildLinks)):
                        if jobName == buildLinks[k][2]:
                            buildLinks[k][1] = artifactsUrl
            break
    return buildLinks

def getBadges():
    badgeBuffer = ""
    badgeBaseUrl = "https://img.shields.io/github/"
    repoPiece = user + "/" + projectTitle
    badgeList = [
        ["pipeline-status", gitLabRepo + "badges/" + gitLabBranch + "/pipeline.svg", gitLabPipelines],
        ["tag", badgeBaseUrl + "tag/" + repoPiece + ".svg", gitHubRepo + "/releases"],
        ["downloads", badgeBaseUrl + "downloads/" + repoPiece + "/total.svg", gitHubRepo + "/releases"]
    ]
    for badge in badgeList:
        badgeBuffer += "[![" + badge[0] + "](" + badge[1] + ")](" + badge[2] + ")\n"
    return badgeBuffer

def main():
    buildLinks = getBuildLinks()
    mdBuffer = "# " + projectTitle + "\n"
    mdBuffer += getBadges() + "\n"
    mdBuffer += projectDesc + "\n\n"
    mdBuffer += "![latest-screenshot](" + screenshotUrl + ")\n\n"
    mdBuffer += "## Links\n"
    for link in linkList:
        mdBuffer += "* [" + link[0] + "](" + link[1] + ")\n"
    mdBuffer += "\n## Build Artifacts\n"
    for build in buildLinks:
        mdBuffer += "* [" + build[0] + "](" + build[1] + ")\n"
    mdBuffer += "\n## Changelog\n"
    with open(changelogPath, 'r') as changelogMd:
        mdBuffer += changelogMd.read() + "\n\n"
    with open(timesheetPath, 'r') as timesheetMd:
        mdBuffer += timesheetMd.read() + "\n\n"
    writeBuffer(mdBuffer, "README.md")

if __name__ == "__main__": main()
