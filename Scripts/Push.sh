#!/bin/bash

# Very simple script to commit and push to GitHub, GitLab, and local Git Server.
# This script assumes the repos, usernames, and credentials are the same.

username=barrettotte
repo=barrettotte/Project_Ranger
projectLocation=../



repo_action(){
  if git $1 "https://${username}:${password}@${2}/${repo}" $branch ; then
    echo "Successfully ${1}ed from/to ${2}"
  else
    echo "Problem occurred ${1}ing to ${2}"
  fi
}

read -p "Enter commit message: " commit
echo "Note: Documentationa generation is only supported in Windows push script."
read -p "Test and Build with GitLab? (y/n) : " ciChoice

case $ciChoice in
  [Yy]*) echo "Triggering CI." ;;
  [Nn]*) commit="${commit} [ci skip]" ;;
  * ) echo "Bad input" ; exit ;;
esac

echo "Commit Message:  ${commit}"
read -p "Enter branch to push to: " branch
read -s -p "Enter Password : " password
cd $projectLocation
git add .

if git commit -m "${commit}" ; then
  repo_action pull github.com
  repo_action push github.com
  repo_action pull gitlab.com
  repo_action push gitlab.com
else
  echo "Problem with commit message."
fi

echo "Script Ended."
exit