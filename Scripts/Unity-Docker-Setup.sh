#!/bin/bash

# Setup docker container for Unity

UNITY_VERSION=latest
USERNAME=barrettotte
PASSWORD=password123
IMAGE_NAME=project_ranger

docker run -it --rm \
-e "UNITY_USERNAME=something@email.com" \
-e "UNITY_PASSWORD=$PASSWORD" \
-e "TEST_PLATFORM=linux" \
-e "WORKDIR=/root/project" \
-v "$(pwd):/root/project" \
registry.gitlab.com/$USERNAME/$IMAGE_NAME:$UNITY_VERSION \
bash
