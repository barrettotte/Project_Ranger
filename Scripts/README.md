# Project_Ranger / Scripts /

A handful of generation and utility scripts to make life easier.

| **Script Name**           | **Description**                     |
| ------------------------- | ----------------------------------- |
| [Backup.py](https://github.com/barrettotte/Project_Ranger/blob/master/Scripts/Backup.py)                         | Create local zip backups of project |
| [Generate-Changelog.py](https://github.com/barrettotte/Project_Ranger/blob/master/Scripts/Generate-ChangeLog.py) | Generate XML, and 3 markdown changelog files based off of GitLab Milestones. |
| [Generate-README.py](https://github.com/barrettotte/Project_Ranger/blob/master/Scripts/Generate-README.py)       | Combine various generated markdown to make root README file. |
| [Push.sh](https://github.com/barrettotte/Project_Ranger/blob/master/Scripts/Push.sh)                             | Simple version of push script that only commits/pushes and has no generation functionality. |
| [Revert.bat](https://github.com/barrettotte/Project_Ranger/blob/master/Scripts/Revert.bat)                       | Lazy script for reverting changes to last commit. |
| [Unity-Docker-Setup.sh](https://github.com/barrettotte/Project_Ranger/blob/master/Scripts/Unity-Docker-Setup.sh) | Setup custom docker container for Unity |
| [../Pull.bat](https://github.com/barrettotte/Project_Ranger/blob/master/Pull.bat)                                | Lazy script for pulling from repo |
| [../Push.bat](https://github.com/barrettotte/Project_Ranger/blob/master/Push.bat)                                | Master script for committing to repos, generating documentation, and triggering CI. |