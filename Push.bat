@ECHO OFF
REM Simple script to commit and push to github, gitlab, and local git server to enable "mirroring" repos.
REM This script assumes the repos, usernames, and credentials are the same.
REM
REM Password masking using inline PowerShell, thanks to StackOverflow
REM   https://stackoverflow.com/questions/664957/can-i-mask-an-input-text-in-a-bat-file
REM
REM Needed in PATH:  python, pip, git, mtc (ManicTime), doxygen
REM 
REM TODO: Add a config file for configuring all scripts


SET projectName=Project_Ranger
SET username=barrettotte
SET repo=barrettotte/%projectName%
SET divider=====================================

SET dateStr=%date:~10,4%-%date:~4,2%-%date:~7,2%
SET mtRaw=ManicTimeData_%dateStr%
SET startDate=2018-08-30
SET endDate=%dateStr% 

SET doxygenConfig="%CD%/Files/Doxyfile"

ECHO %divider%
ECHO %projectName% Push Script
ECHO %divider%
ECHO.

SET /p issueNumber="Enter issue number: "
SET /P commit="Enter commit message: "

REM TODO: Separate out into small functions!
SET /P genChoice="Generate documentation files? (y/n) : "
if "%genChoice%" == "y" (
    ECHO Executing Generate-ChangeLog.py
    python Scripts\Generate-ChangeLog.py
    ECHO Done.
    ECHO Exporting ManicTime data
    mtc export ManicTime/Applications "%CD%\Scripts\%mtRaw%.csv" /fd:%startDate% /td:%endDate%
    ECHO Executing Generate-TimeSheet.py
    python Scripts\Generate-TimeSheet.py Scripts\%mtRaw%.csv
    DEL Scripts\%mtRaw%.csv
    ECHO Done.
    ECHO Generating documentation using Doxygen
    DEL /s /q "%CD%\docs\Doxygen-html\*" >nul
    RMDIR /s /q "%CD%\docs\Doxygen-html\"
    doxygen %doxygenConfig% >nul
    MOVE "%CD%\docs\html" "%CD%\docs\Doxygen-html"
    ECHO Done.
)

ECHO Generating README.md
python Scripts/Generate-README.py
ECHO Done.

SET /p zipChoice="Create a backup zip of project? (y/n) : "
if "%zipChoice%" == "y" (
    ECHO Executing Backup.py
    python Scripts\Backup.py
    ECHO Done.
)

SET /P ciChoice="Test and Build with GitLab? (y/n) : "
if "%ciChoice%" == "y" (
    ECHO GitLab-CI Enabled.
) else (
    SET commit=%commit% [ci skip]
)
SET commit=[#%issueNumber%] %commit%
ECHO Commit Message:  "%commit%"
ECHO.

SET /P branch="Enter branch to push to: "
SET "psCommand=powershell -Command "$pword = read-host 'Enter Password' -AsSecureString ; ^
    $BSTR=[System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($pword); ^
    [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)""
FOR /f "usebackq delims=" %%p in (`%psCommand%`) do SET pass=%%p

git add .
git commit -m "%commit%"

IF %ERRORLEVEL%==1 (
    ECHO Error occurred with commit %commit%
    GOTO END
)

CALL:PUSH github
CALL:PUSH gitlab

:END
    ECHO Script ended.
    PAUSE
    EXIT

:PUSH
    git pull https://%username%:%pass%@%~1.com/%repo% %branch%
    git push https://%username%:%pass%@%~1.com/%repo% %branch%
    IF %ERRORLEVEL%==0 (
        ECHO Successfully pushed to %~1.com/%repo% %branch%
        ECHO.
    ) ELSE (
        ECHO Error occurred pushing to %~1.com/%repo% %branch%
    GOTO:EOF
)